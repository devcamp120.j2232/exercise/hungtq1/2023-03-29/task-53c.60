import models.BigDog;
import models.Cat;
import models.Dog;

public class App {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Mun");
        Dog dog = new Dog("Kun");
        BigDog bigDog = new BigDog("KunL");
        System.out.println("-- Cat --");
        cat.greets();

        System.out.println("-- Dog --");
        dog.greets();
        dog.greets(dog);

        System.out.println("-- BigDog --");
        bigDog.greets();
        bigDog.greets(dog);
        bigDog.greets(bigDog);
    }
}
